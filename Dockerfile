#
# Haiku buildmaster status
#
# Local development
#   - docker build . -t buildstatus:next
#   - docker run -p 4500:4567 -v ~/x86-buildmaster:/data buildstatus:next
#

FROM fedora
MAINTAINER Haiku, Inc.

RUN dnf update -y && dnf install -y ruby rubygem-sinatra

ENV APP_HOME /app
ENV DATA_HOME /data
ENV BASE_DIR $DATA_HOME/haikuports/buildmaster/output/buildruns

RUN mkdir $APP_HOME && mkdir $DATA_HOME
COPY . $APP_HOME

WORKDIR $APP_HOME

ENV PORT 4567
EXPOSE 4567

CMD ["ruby", "server.rb", "-o", "0.0.0.0"]
