# Haiku Build Monitor

A public dashboard of the Haiku buildmaster

## Environment

* BASE_DIR needs to be set to the buildmaster result directory
* BASE_HTTP can be adjusted if buildmonitor is behind a reverse proxy

## Example Startup

``BASE_DIR=/builds/buildmaster.x86_gcc2/buildmaster/buildruns BASE_HTTP="/new/x86_gcc2" ruby server.rb -o 127.0.0.1 -p 8032``

## License

Copyright 2016-2017, Haiku, Inc.
Relased under the terms of the MIT license.
