require 'sinatra'
require 'json'
require 'pp'
require './libs/builders.rb'
require './helpers/visual.rb'

helpers VisualHelpers

configure do
  set :prefix => (ENV["BASE_DIR"] or "/home/kallisti5/buildruns")
  set :httpbase => (ENV["BASE_HTTP"] or "")
  set :cache => Array.new
  set :timeout => 15
  set :lock => true
end

class String
    def is_i?
       !!(self =~ /\A[-+]?[0-9]+\z/)
    end
end

def cache_expired?(index)
	timestamp = settings.cache[index.to_i].fetch(:date, 0)
	(Time.now - timestamp) > settings.timeout
end

def get_buildrun(index)
	if !settings.cache or !settings.cache[index.to_i] or cache_expired?(index)
        if !File.file?("#{settings.prefix}/#{index}/status.json")
          return nil
        end
		data = JSON.parse(File.read("#{settings.prefix}/#{index}/status.json"))
		settings.cache[index.to_i] = { date: Time.now, data: data }
	end
	return settings.cache[index.to_i][:data]
end

def get_buildrun_numbers()
  now = Time.now.to_i
  result = Array.new
  File.read("#{settings.prefix}/buildruns.txt").split("\n").reverse.each do |build|
    if !File.file?("#{settings.prefix}/#{build}/status.json")
      next
    end
    buildrun = get_buildrun(build.to_i)
    if result.count < 50
      finished = now - buildrun.fetch("endTime", Time.now).to_i
    else
      finished = -1
    end
    result.push({name: build, age: finished})
  end
  result
end

before '/*' do
  # Used by layout menu
  @buildrun_numbers = get_buildrun_numbers()
  @httpbase = settings.httpbase
end

get '/build/:id' do
  if !params[:id] or !params[:id].is_i?
    status 400
    return {error: "Invalid build id: #{params[:id]}"}.to_json
  end

  @buildrun = get_buildrun(params[:id])
  @id = params[:id]
  @prefix = settings.prefix
  @builders = Builders.new(@buildrun.fetch("builders", {}))
  @builds = @buildrun.fetch("builds", {})
  erb :build
end

get '/build' do
  erb :builds
end

get '/' do
  erb :overview
end
