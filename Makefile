default:
	podman build . -t docker.io/haiku/buildstatus:latest
push:
	podman push docker.io/haiku/buildstatus:latest
test:
	podman run -P -v buildmaster-x86_64:/data docker.io/haiku/buildstatus:latest
