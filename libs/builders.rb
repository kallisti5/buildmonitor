class Builder
  attr_reader :type
  attr_reader :name
  attr_reader :state
  attr_reader :current_build
  def initialize(type, params)
    @type = type
    @name = params.fetch("name", "unknown")
    @state = params.fetch("state", "unknown")
    @current_build = (params.fetch("currentBuild", {}) or {}).fetch("build", {})
  end
end

class Builders
  attr_accessor :builders
  def initialize(params)
    @builders = []

    return if !params
    params.each do |key,value|
      value.each do |builder|
        @builders.push(Builder.new(key.to_s, builder))
      end
    end
  end

  def all()
    @builders
  end

  def type(type_name)
    @builders.select {|b| b.type == type_name}
  end
end
