module VisualHelpers
  def build_status_icon(status)
    image = case status
      when "complete" then "check_circle"
	  when "failed" then "error"
	  when "lost" then "warning"
      when "active" then "loop"
      when "blocked" then "snooze"
      when "scheduled" then "schedule"
      else "done"
    end
    return "<i class='material-icons' style='vertical-align: middle;'>#{image}</i>&nbsp;"
  end

  def builder_status_icon(builder)
    image = case builder.type.downcase
      when "active" then "phonelink"
      when "reconnecting" then "phonelink"
      when "idle" then "phonelink"
      else "phonelink_off"
    end
    color = case builder.type.downcase
      when "active" then "red"
      when "reconnecting" then "blue"
      when "idle" then "green"
      else "orange"
    end
    html = "<div class='chip #{ color }-text'>"
    html += "<i class='material-icons' style='vertical-align: middle;'>#{image}</i>&nbsp;#{builder.state}</div>"
    return html
  end

  def short_ago(age)
    if age < 60
      return "#{age} second#{(age != 1) ? 's' : ''} ago"
    elsif age < (60 * 60)
      mins = age / 60
      return "#{mins} minute#{(mins != 1) ? 's' : ''} ago"
    elsif age < (60 * 60 * 24)
      hours = age / (60 * 60)
      return "#{hours} hour#{(hours != 1) ? 's' : ''} ago"
    elsif age < (60 * 60 * 24 * 7)
      days = age / (60 * 60 * 24)
      return "#{days} day#{(days != 1) ? 's' : ''} ago"
    else
      weeks = age / (60 * 60 * 24 * 7)
      return "#{weeks} week#{(weeks != 1) ? 's' : ''} ago"
    end
  end
end
